#!/bin/bash

sudo apt update
sudo apt upgrade


# install asdf recomended dependencies
sudo apt install -y \
  automake autoconf libreadline-dev \
  libncurses-dev libssl-dev libyaml-dev \
  libxslt-dev libffi-dev libtool unixodbc-dev \
  unzip curl

# install asdf runtime version manager
echo "Installing asdf ......."
if [[ -z $(ls ~/.asdf) ]];then
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.1
    echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.profile
    echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc
fi

# install asdf nodejs plugin
if [[ -z $(type -P node)]]; then
echo "Installing nodejs ..."
sudo apt install -y dirmanager
sudo apt install -y gpg
asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs.git
bash ~/.asdf/plugins/nodejs/bin/import-release-team-keyring
echo "completed nodejs installation!!"
fi

# install Ruby and rails dependencies
if [[ -z $(type -P ruby) ]]; then
echo "Installing Ruby and Rails dependencies"
sudo apt -y install git-core curl zlib1g-dev build-essential libssl-dev \
     libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev \
     libcurl4-openssl-dev python-software-properties libffi-dev software-properties-common

# install asdf ruby plugin
echo "installing asdf ruby plugin..... "
asdf plugin-add ruby https://github.com/asdf-vm/asdf-ruby.git
asdf install ruby 2.6.3
asdf global ruby 2.6.3
fi

# install asdf elixir plugin
if [[ -z $(type -P elixir) ]]; then
echo "installing asdf elixir plugin...... "
asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir.git
asdf install elixir 1.8.2
asdf global elixir 1.8.2
echo "Elixir installation complete......"
fi

# install hashicorp asdf plugin
echo "installing asdf Hashicorp plugin...... "
asdf plugin-add consul|nomad|packer|serf|terraform|vault https://github.com/Banno/asdf-hashicorp.git
asdf terraform install 0.12.3

# install asdf kubectl plugin
if [[ -z $(type -P kubectl) ]]; then
echo "installing asdf kubectl plugin...... "
asdf plugin-add kubectl https://github.com/Banno/asdf-kubectl.git
asdf install kubectl 1.15.0
asdf global kubectl 1.15.0
echo "Installed kubectl......"
fi

#install asdf gradle
if [[ -z $(type -P gradle) ]]; then
echo "installing asdf gradle plugin"
asdf plugin-add gradle
asdf install gradle 5.4.1
asdf global gradle 5.4.1
echo "Installed gradle...."
fi

# install asdf docker-slim plugin
echo "installing asdf docker-slim plugin...... "
asdf plugin-add docker-slim https://github.com/everpeace/asdf-docker-slim.git

# install asdf java plugin
if [[ -z $(asdf plugin-list | grep java) ]]; then
echo "installing asdf java plugin...... "
asdf plugin-add java

# install java
asdf install java openjdk-11.0.1
asdf global java openjjdk-11.0.1

# reshim asdf
asdf reshim
fi

#install asdf golang
if [[ -z $(type -P gradle) ]]; then
    echo "installing asdf golang plugin"
    asdf plugin-add golang https://github.com/kennyp/asdf-golang.git
    asdf install golang 1.13.5
    asdf global golang 1.13.5
    echo "Installed golang 1.13.5...."
fi

PYTHON_VERSION=3.8.12
# install pyenv virtualenv using install script
if [[ -z $(type -P pyenv) ]]; then
    echo "Installing pyenv with python $PYTHON_VERSION"
    curl https://pyenv.run | bash
    echo 'export PATH="~/.pyenv/shims:$PATH"' >> ~/.bashrc
    echo 'export PATH="~/.pyenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(pyenv init -)"' >> .bashrc
    echo 'eval "$(pyenv virtualenv-init -)"' >> .bashrc
    source ~/.bashrc
    pyenv install $PYTHON_VERSION
    pyenv global $PYTHON_VERSION
    echo "Completed installing pyenv with python $PYTHON_VERSION"
fi

#http://caffe.berkeleyvision.org/install_apt.html
#install caffee deps
sudo apt -y build-dep caffe-cpu        # dependencies for CPU-only version
# sudo apt build-dep caffe-cuda       # dependencies for CUDA version

# install meld
sudo apt-get -y install meld

#a ssh key
mkdir -p ~/.ssh && cd ~/.ssh
if [ -z "$(ls $HOME/.ssh | grep 'id_rsa.pub')" ]; then
    ssh-keygen -t rsa -b 4096 -C "davidfradis@gmail.com"
fi

#install vim
mkdir -p /media/data/local_machine_setup && cd /media/data/local_machine_setup
if [[ -z $(ls /media/data/local_machine_setup/dotvim2) ]];then
    cd /media/data/local_machine_setup/dotvim2
    git pull origin master
    maker
else
    git clone git@github.com:vitaly/dotvim2.git
    cd dotvim2 && make
fi

# install spacemacs
if [[ -z $(type -P emacs) ]]; then
	sudo add-apt-repository ppa:kelleyk/emacs
    sudo apt update
    sudo apt -y install emacs26
	  mv ~/.emacs ~/.emacs.old
	  mv ~/.emacs.d ~/.emacs.d.old
	  git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d
fi

# install docker
if [[ -z $(type -P docker) ]]; then
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository -y \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
         stable"
    sudo apt-get update
    sudo apt-get -y install docker-ce
fi

if [[ -z $(cut -d: -f1 /etc/group | grep docker)  ]]; then
    sudo groupadd docker
fi
if [[ -z $(cut -d: -f1 /etc/passwd | grep docker) ]]; then
    sudo usermod -aG docker $USER
fi

#install docker-machine
if [[ -z $(type -P docker-machine) ]]; then
    curl -L https://github.com/docker/machine/releases/download/v0.10.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine &&
    chmod +x /tmp/docker-machine &&
    sudo cp /tmp/docker-machine /usr/local/bin/docker-machine
fi

# install docker-compose
if [[ -z $(type -P docker-compose) ]]; then
    sudo curl -L "https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi

# soft links to config files
if [ -e /media/data/local_machine_setup/dotfiles/.spacemacs ];then
	if ! [ -e ~/.spacemacs ]; then
		ln -s /media/data/local_machine_setup/dotfiles/.spacemacs ~/.spacemacs
		echo "softlink created /media/froy001/local-data/local_machine_setup/dotfiles/.spacemacs"
	fi
fi
if [ -e /media/data/local_machine_setup/dotfiles/terminator_config ]; then
	mkdir -p ~/.config/terminator
	ln -s /media/data/local_machine_setup/dotfiles/terminator_config ~/.config/terminator/config
fi

# install gcloud SDK
if [ [-z $(type -p gcloud) ]]; then
    # Create an environment variable for the correct distribution
    echo "export CLOUD_SDK_REPO=\"cloud-sdk-$(lsb_release -c -s)\"" >> ~/.profile \
        && source ~/.profile

    # Add the Cloud SDK distribution URI as a package source
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

    # Import the Google Cloud Platform public key
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

    # Update the package list and install the Cloud SDK
    sudo apt-get update && sudo apt-get install -y google-cloud-sdk
fi

# install graphviz
if [[ -z $(type -P dot) ]]; then
    sudo apt update && sudo apt install -y graphviz
fi

# install aws-cli
if [[ -z type -P aws ]]; then
    sudo apt update && sudo apt -y install awscli
fi

# install gcloud and kubectl
if [[ -z type -P gcloud]]; then
    echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
    sudo apt-get install -y apt-transport-https ca-certificates gnupg
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
    sudo apt-get update && sudo apt-get install -y google-cloud-sdk kubectl
fi

# install Source Code Pro Fonts
cd ~/Downloads
wget https://github.com/adobe-fonts/source-code-pro/archive/2.030R-ro/1.050R-it.zip

if [ ! -d "~/.fonts" ] ; then
    mkdir ~/.fonts
fi

unzip 1.050R-it.zip

cp source-code-pro-*-it/OTF/*.otf ~/.fonts/
rm -rf source-code-pro*
rm 1.050R-it.zip

cd ~/

fc-cache -f -v

# Erlang dependencies
if [[ -z $(type -P xsltproc) ]]; then
    sudo apt -y install xsltproc
fi
if [[ -z $(type -P libncurses-dev)]]; then
    sudo apt -y install libncurses-dev
fi
if [[ -z $(type -P automake)]]; then
    sudo apt -y install automake
fi
if [[ -z $(type -P autoconf)]]; then
    sudo apt -y install autoconf
fi
if [[ -z $(type -P fop)]]; then
    sudo apt -y install fop
fi
# wxWidget especially for Erlang compilation
if [[ -z $(cat /etc/apt/sources.list | grep "deb https://repos.codelite.org/wx3.1.3/ubuntu/ bionic universe") ]]; then

    sudo apt-key adv --fetch-keys https://repos.codelite.org/CodeLite.asc
    sudo apt-add-repository 'deb https://repos.codelite.org/wx3.1.3/ubuntu/ bionic universe'
    sudo apt-get update
    sudo  apt-get install libwxbase3.1-0-unofficial3 \
          libwxbase3.1-dev \
          libwxgtk3.1\
          libwxgtk3.1-dev \
          wx3.1-headers \
          wx-common \
          libwxgtk-media3.1 \
          libwxgtk-media3.1-dev \
          libwxgtk-webview3.1\
          libwxgtk-webview3.1-dev \
          libwxbase3.1-dbg \
          libwxgtk3.1-dbg \
          libwxgtk-webview3.1-dbg \
          libwxgtk-media3.1-dbg \
          wx3.1-i18n \
          sudo ln -sv wx-3.1/wx wx

fi
# install asdf erlang plugin
if [[ -z $(type -P erlang)]]; then
    echo "installing asdf erlang plugin...... "
    asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang.git
    asdf install erlang 22.0
    asdf global erlang 22.0
fi
# install drive-google https://github.com/odeke-em/drive
if [[ -z $(type -P drive-google) ]]; then
    echo "Installing drive-google....."
    go get -u github.com/odeke-em/drive/drive-google
    asdf reshim
    drive-google init /media/data/gdrive
fi
