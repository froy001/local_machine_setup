# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
# if [ -n "$BASH_VERSION" ]; then
#     # include .bashrc if it exists
#     if [ -f "$HOME/.bashrc" ]; then
# 	      source "$HOME/.bashrc"
#     fi
# fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# set exercism autocomplete
if [ -f ~/.config/exercism/exercism_completion.bash ]; then
	source ~/.config/exercism/exercism_completion.bash
fi
. $HOME/.asdf/asdf.sh
. $HOME/.asdf/plugins/java/asdf-java-wrapper.bash

#Grunt Autocomplete
#eval "$(grunt --completion=bash)"

# >>> conda initialize >>>
# # !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('/home/froy001/.asdf/installs/python/miniconda3-latest/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "/home/froy001/.asdf/installs/python/miniconda3-latest/etc/profile.d/conda.sh" ]; then
#         . "/home/froy001/.asdf/installs/python/miniconda3-latest/etc/profile.d/conda.sh"
#     else
#         export PATH="/home/froy001/.asdf/installs/python/miniconda3-latest/bin:$PATH"
#     fi
# fi
# unset __conda_setup
# <<< conda initialize <<<


# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# org-redmine-auth-api-key
export ORG_REDMINE_API_KEY="1655556aad2c9377467084398329956abc277b6d"

# Spotify spacemacs layer credentials
export SPOTIFY_LAYER_CLIENT_ID=9323079e3c3e4364a3eaf45b815b66bc
export SPOTIFY_LAYER_SECRET=a5d73b43ef2b4a55b415e3247637c19d

# Google calendar api creds
export GOOGLE_CALENDAR_API_ID=815682894589-u6i7oo0mipkti0cnclvstoirejt2osge.apps.googleusercontent.com
export GOOGLE_CALENDAR_API_SECRET=bx7YIRqczh_EWaIcuQZ0o-CX

# pgp
export GPGKEY=FF7EDC8474E04F86

# gcloud user
export GCLOUD_USER="cybord"

# Cybord creds
export GOOGLE_APPLICATION_CREDENTIALS=/home/froy001/.config/gcloud/terraform-admin.json
export GOOGLE_PROJECT=cybord-terraform-admin

# Vagrant
export VAGRANT_HOME=/media/froy001/BF75-8B8C/vm_disks/vagrant_home
export VAGRANT_EXPERIMENTAL="disks"

######### WP_LOCAL_DOCKER aliases https://github.com/10up/wp-local-docker#docker-compose-overrides-file ############
# WP-CLI
alias dcwp='docker-compose exec --user www-data phpfpm wp'
# SSH into container
alias dcbash='docker-compose exec --user root phpfpm bash'
# Kill all docker containers that are running
docker-stop() { docker stop $(docker ps -a -q); }
# Stop all dockers and docker-compose up
alias dup="docker-stop && docker-compose up -d"

export PATH="$HOME/.poetry/bin:$PATH"
