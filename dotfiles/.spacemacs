;; -*- mode: emacs-lisp -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration.
You should not put any user code in this function besides modifying the variable
values."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs
   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused
   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t
   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(rust
     ansible
     nginx
     windows-scripts
     (tern :variables
           tern-command '("node" "/home/froy001/.asdf/shims/tern")
           tern-disable-port-files nil
           )
     (php :variables php-backend 'lsp)
     erlang
     phoenix
     csv
     csharp
     docker
     lsp
     yaml
     html
     (ruby :variables
           enh-ruby-mode t
           ruby-version-manager 'rvm
           ruby-backend 'robe)
     ruby-on-rails
     (python :variables
             python-enable-yapf-format-on-save t
             python-backend 'lsp
             python-lsp-server 'pyright
             python-test-runner 'pytest
             python-formatter 'black
             python-format-on-save t)
     java
     javascript
     react
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
     ;; <M-m f e R> (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     ;;helm
     auto-completion
     better-defaults
     emacs-lisp
     git
     markdown
     (org :variables
          org-directory "~/Dropbox/org"
          org-log-done t
          org-startup-with-inline-images t
          org-agenda-weekend-days '(5,6)
          org-support-shift-select t
          org-enable-github-support t
          org-enable-org-journal-support t
          org-journal-dir "~/Dropbox/org/journal/"
          org-journal-file-format "%Y-%m-%d"
          org-journal-date-prefix "#+TITLE: "
          org-journal-date-format "%A, %B %d %Y"
          org-journal-time-prefix "* "
          org-journal-time-format ""
          org-projectile-file "TODOs.org"
          )
     (shell :variables
            shell-default-shell 'shell
            shell-default-height 30
            shell-default-position 'bottom)
     spell-checking
     syntax-checking
     version-control
     terraform
     (go :variables
         go-use-gometalinter t
         go-tab-width 4)
     (sql :variables
          sql-capitalize-keywords t
          sql-capitalize-keywords-blacklist '("name" "varchar"))
     spotify
     (ivy :variables ivy-enable-advanced-buffer-information t)
     ;; (treemacs :variables
     ;;           treemacs-use-follow-mode 'tag
     ;;           treemacs-use-filewatch-mode t
     ;;           treemacs-use-git-mode 'deferred
     ;;           treemacs-use-collapsed-directories 3
     ;;           treemacs-lock-width t)

     gnus
     google-calendar
     pdf
     multiple-cursors
     search-engine
     )
   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages '(yasnippet-snippets
                                      org-redmine
                                      elixir-mode
                                      eglot)
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()
   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '(alchemist)
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and uninstall any
   ;; unused packages as well as their unused dependencies.
   ;; `used-but-keep-unused' installs only the used packages but won't uninstall
   ;; them if they become unused. `all' installs *all* packages supported by
   ;; Spacemacs and never uninstall them. (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration.
You should not put any user code in there besides modifying the variable
values."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   dotspacemacs-elpa-timeout 5
   ;; If non nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil
   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'.
   dotspacemacs-elpa-subdirectory nil
   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'hybrid
   ;; If non nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official
   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'."
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7)
                                (agenda))
   ;; True if the home buffer should respond to resize events.
   dotspacemacs-startup-buffer-responsive t
   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode
   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press <SPC> T n to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(spacemacs-dark
                         spacemacs-light)
   ;; If non nil the cursor color matches the state color in GUI Emacs.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   dotspacemacs-default-font '("Source Code Pro"
                               :size 13
                               :weight normal
                               :width normal
                               :powerline-scale 2)
   ;; The leader key
   dotspacemacs-leader-key "SPC"
   ;; The key used for Emacs commands (M-x) (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"
   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"
   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs C-i, TAB and C-m, RET.
   ;; Setting it to a non-nil value, allows for separate commands under <C-i>
   ;; and TAB or <C-m> and RET.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil
   ;; If non nil `Y' is remapped to `y$' in Evil states. (default nil)
   dotspacemacs-remap-Y-to-y$ nil
   ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
   ;; there. (default t)
   dotspacemacs-retain-visual-state-on-shift t
   ;; If non-nil, J and K move lines up and down when in visual mode.
   ;; (default nil)
   dotspacemacs-visual-line-move-text nil
   ;; If non nil, inverse the meaning of `g' in `:substitute' Evil ex-command.
   ;; (default nil)
   dotspacemacs-ex-substitute-global nil
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil
   ;; If non nil then the last auto saved layouts are resume automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil
   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1
   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache
   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize nil
   ;; if non nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header nil
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; Controls fuzzy matching in helm. If set to `always', force fuzzy matching
   ;; in all non-asynchronous sources. If set to `source', preserve individual
   ;; source settings. Else, disable fuzzy matching in all sources.
   ;; (default 'always)
   dotspacemacs-helm-use-fuzzy 'always
   ;; If non nil the paste micro-state is enabled. When enabled pressing `p`
   ;; several times cycle between the kill ring content. (default nil)
   dotspacemacs-enable-paste-transient-state nil
   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4
   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom
   ;; If non nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t
   ;; If non nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup t
   ;; If non nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90
   ;; If non nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t
   ;; If non nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t
   ;; If non nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols t
   dotspacemacs-mode-line-theme 'spacemacs
   ;; If non nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t
   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers t
   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil
   ;; If non-nil smartparens-strict-mode will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil
   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `ag', `pt', `ack' and `grep'.
   ;; (default '("ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now. (default nil)
   dotspacemacs-default-package-repository nil
   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed'to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'trailing
    ))

(defun dotspacemacs/user-init ()
  "Initialization function for user code.
It is called immediately after `dotspacemacs/init', before layer configuration
executes.
 This function is mostly useful for variables that need to be set
before packages are loaded. If you are unsure, you should try in setting them in
`dotspacemacs/user-config' first."
  (setenv "WORKON_HOME" "/home/froy001/.asdf/installs/python/miniconda3-latest/envs")
  ;; Get email, and store in nnml
   (setq gnus-secondary-select-methods
         '(
           (nnimap "gmail"
                   (nnimap-address
                    "imap.gmail.com")
                   (nnimap-server-port 993)
                   (nnimap-stream ssl))
           ))

   ;; Send email via Gmail:
   (setq message-send-mail-function 'smtpmail-send-it
         smtpmail-default-smtp-server "smtp.gmail.com")

   ;; Archive outgoing email in Sent folder on imap.gmail.com:
   (setq gnus-message-archive-method '(nnimap "imap.gmail.com")
         gnus-message-archive-group "[Gmail]/Sent Mail")

   ;; set return email address based on incoming email address
   (setq gnus-posting-styles
         '(((header "to" "address@outlook.com")
            (address "address@outlook.com"))
           ((header "to" "address@gmail.com")
            (address "address@gmail.com"))))

   ;; store email in ~/gmail directory
   (setq nnml-directory "~/gmail")
   (setq message-directory "~/gmail")
   (setq smtpmail-smtp-service 587)
   ;; set gnus-parameter
   (setq gnus-parameters
         '(("nnimap.*"
            (gnus-use-scoring nil)
            (expiry-wait . 2)
            (display . all))))

  )

(defun term-send-up    () (interactive) (term-send-raw-string "\e[A"))
(defun term-send-down  () (interactive) (term-send-raw-string "\e[B"))
(defun term-send-right () (interactive) (term-send-raw-string "\e[C"))
(defun term-send-left  () (interactive) (term-send-raw-string "\e[D"))

(defun dotspacemacs/user-config ()

  "Configuration function for user code.
This function is called at the very end of Spacemacs initialization after
layers configuration.
This is the place where most of your configurations should be done. Unless it is
explicitly specified that a variable should be set before a package is loaded,
you should place your code here."

  (require 'eglot)

  ;; This is optional. It automatically runs `M-x eglot` for
  ;; you whenever you are in `elixir-mode`
  (add-hook 'elixir-mode-hook 'eglot-ensure)

  ;; Make sure to edit the path appropriately,
  ;; use the .bat script instead for Windows
  (add-to-list 'eglot-server-programs '(elixir-mode "/media/data/projects/elixir/elixir-ls/release/language_server.sh"))
  ;; Create a buffer-local hook to run elixir-format on save, only when we enable elixir-mode.
  (add-hook 'elixir-mode-hook
            (lambda () (add-hook 'before-save-hook 'elixir-format nil t)))
  (add-hook 'elixir-format-hook (lambda ()
                                  (if (projectile-project-p)
                                      (setq elixir-format-arguments
                                            (list "--dot-formatter"
                                                  (concat (locate-dominating-file buffer-file-name ".formatter.exs") ".formatter.exs")))
                                    (setq elixir-format-arguments nil))))
  ;; (require 'helm-bookmark)
  (defun my-csharp-mode-setup ()
    (setq indent-tabs-mode nil)
    (setq c-syntactic-indentation t)
    (c-set-style "ellemtel")
    (setq c-basic-offset 4)
    (setq truncate-lines t)
    (setq tab-width 4)
    (setq evil-shift-width 4)
    (setq evil-ex-visual-char-range t)
    (local-set-key (kbd "C-c C-c") 'recompile))
  (setq spaceline-org-clock-p t)
  (add-hook 'csharp-mode-hook 'my-csharp-mode-setup t)
  ;; this evoids issues with the emacs loaded package
  (with-eval-after-load 'org

    ;; here goes your Org config :)
    ;; ....
    (org-babel-do-load-languages 'org-babel-load-languages '(
                                                             (ruby . t)
                                                             (python . t)
                                                             (elixir . t)
                                                             (java . t)
                                                             ))
    ;; Add files to agenda
    (setq org-agenda-files '("~/Dropbox/org/01_inbox.org"
                             "~/Dropbox/org/02_gtd.org"
                             "~/Dropbox/org/04_tickler.org"
                             "~/Dropbox/org/davidfradis_calendar.org"))

    ;; Setup capture templates for todo and tickler
    (setq org-capture-templates '(("t" "Todo [inbox]" entry
                                   (file+headline "~/Dropbox/org/01_inbox.org" "Tasks")
                                   "* TODO %i%?")
                                  ("T" "Tickler" entry
                                   (file+headline "~/Dropbox/org/04_tickler.org" "Tickler")
                                   "* %i%? \n %U")
                                  ("p" "Project" entry
                                   (file+headline "~/Dropbox/org/02_gtd.org" "Projects")
                                   (file "~/Dropbox/org/template_files/tpl_project.org"))))

    ;; set refile targets for GTD
    (setq org-refile-targets '(("~/Dropbox/org/02_gtd.org" :maxlevel . 3)
                               ("~/Dropbox/org/03_someday.org" :level . 1)
                               ("~/Dropbox/org/04_tickler.org" :maxlevel . 2)))
    (setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w@/!)" "|" "DONE(d!)" "CANCELLED(c@)")))
    ;; org-mode tags used for GTD context
    (setq org-tag-alist '(("@office" . ?o)
                          ("@home" . ?h)
                          ("@phone" . ?p)
                          ("@computer" . ?c)
                          ("@errands" . ?e)
                          ("@traveling" . ?t)))
    ;; agenda custom commands for filtering by context
    (setq org-agenda-custom-commands
          '(("o" "At the office" tags-todo "@office"
             ((org-agenda-overiding-header "Office")
              (org-agenda-skip-functtion #'my-org-agenda-skip-all-siblings-but-first)))
            ("h" "At home" tags-todo "@home"
             ((org-agenda-overriding-header "Home")
              (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)))
            ("p" "Phone" tags-todo "@phone"
             ((org-agenda-overriding-header "Phone")
              (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)))
            ("e" "Errands" tags-todo "@errands"
             ((org-agenda-overriding-header "Errands")
              (org-agenda-skip-function #'my-org-agenda-skip-all-siblings-but-first)))))
    (defun my-org-agenda-skip-all-siblings-but-first ()
      "Skip all but the first non-done entry."
      (let (should-skip-entry)
        (unless (org-current-is-todo)
          (setq should-skip-entry t))
        (save-excursion
          (while (and (not should-skip-entry) (org-goto-sibling t))
            (when (org-current-is-todo)
              (setq should-skip-entry t))))
        (when should-skip-entry
          (or (outline-next-heading)
              (goto-char (point-max))))))

    (defun org-current-is-todo ()
      (string= "TODO" (org-get-todo-state)))
    )
  ;; org-redmine
  (setq org-redmine-uri "http://www.hostedredmine.com/")
  (setq org-redmine-auth-api-key (getenv "ORG_REDMINE_API_KEY"))
  ;;Spotify config
  (setq counsel-spotify-client-id (getenv "SPOTIFY_LAYER_CLIENT_ID"))
  (setq counsel-spotify-client-secret (getenv "SPOTIFY_LAYER_SECRET"))
  ;; google-calendar config
  ;; (setq org-gcal-client-id (getenv "GOOGLE_CALENDAR_API_ID"))
  ;; (setq org-gcal-client-secret (getenv "GOOGLE_CALENDAR_API_SECRET"))
  (setq org-gcal-client-id "815682894589-u6i7oo0mipkti0cnclvstoirejt2osge.apps.googleusercontent.com")
  (setq org-gcal-client-secret "bx7YIRqczh_EWaIcuQZ0o-CX")
  (setq org-gcal-file-alist '(("davidfradis@gmail.com" . "~/Dropbox/org/davidfradis_calendar.org")))
  ;; calfw
  ;; First day of the week
  (setq calendar-week-start-day 0) ; 0:Sunday, 1:Monday
  (setq cfw:org-capture-template '("s" "Scedule an event" entry
                                   (file "~/Dropbox/org/davidfradis_calendar.org
")
                                   "* %^{Description}\n%^{LOCATION}p\n%(cfw:org-capture-day)\n%?"))
  (add-hook 'org-capture-after-finalize-hook 'google-calendar/sync-cal-after-capture)
  ;; Get email, and store in nnml
  (setq gnus-secondary-select-methods
        '(
          (nnimap "gmail"
                  (nnimap-address
                   "imap.gmail.com")
                  (nnimap-server-port 993)
                  (nnimap-stream ssl))
          ))

  ;; Send email via Gmail:
  (setq message-send-mail-function 'smtpmail-send-it
        smtpmail-default-smtp-server "smtp.gmail.com")

  ;; Archive outgoing email in Sent folder on imap.gmail.com:
  (setq gnus-message-archive-method '(nnimap "imap.gmail.com")
        gnus-message-archive-group "[Gmail]/Sent Mail")

  ;; set return email address based on incoming email address
  (setq gnus-posting-styles
        '(((header "to" "address@outlook.com")
           (address "address@outlook.com"))
          ((header "to" "address@gmail.com")
           (address "address@gmail.com"))))

  ;; store email in ~/gmail directory
  (setq nnml-directory "~/gmail")
  (setq message-directory "~/gmail")

  ;; Search engine
  (setq browse-url-browser-function 'browse-url-generic
        engine/browser-function 'browse-url-generic
        browse-url-generic-program "google-chrome")

)
;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files (quote ("~/tmp/test.org")))
 '(org-clock-in-switch-to-state "")
 '(org-clock-out-remove-zero-time-clocks t)
 '(org-edit-src-content-indentation 4)
 '(org-edit-timestamp-down-means-later t)
 '(org-extend-today-until 22)
 '(org-indent-indentation-per-level 4)
 '(org-indent-mode-turns-off-org-adapt-indentation t)
 '(org-list-allow-alphabetical t)
 '(org-list-automatic-rules (quote ((checkbox . t) (checkbox . t))))
 '(org-todo-keyword-faces
   (quote
    (("TODO" . "#dc752f")
     ("WAITING" . "#9323cf")
     ("DONE" . "#2774cc")
     ("CANCELLED" . "#07317d"))))
 '(package-selected-packages
   (quote
    (wgrep smex ivy-hydra flyspell-correct-ivy counsel-projectile counsel swiper ivy spotify helm-spotify-plus multi org-redmine unfill mwim yasnippet auto-complete helm-rails ox-gfm org-super-agenda org-dashboard org-journal yasnippet-snippets yasnippet-classic-snippets phpunit phpcbf php-extras php-auto-yasnippets drupal-mode php-mode sql-indent hcl-mode rake inflections pcre2el spinner org-category-capture log4e gntp org-mime skewer-mode simple-httpd multiple-cursors hydra lv parent-mode request haml-mode gitignore-mode fringe-helper git-gutter+ git-gutter flyspell-correct pos-tip flx highlight transient git-commit with-editor iedit anzu goto-chg undo-tree json-snatcher json-reformat diminish csharp-mode web-completion-data tern inf-ruby bind-map bind-key packed anaconda-mode pythonic elixir-mode pkg-info epl powerline alert go-guru go-eldoc flycheck-gometalinter company-go go-mode eclim dash-functional smartparens evil flycheck company projectile helm helm-core avy markdown-mode org-plus-contrib magit magit-popup async f js2-mode dash s dockerfile-mode docker tablist docker-tramp omnisharp yapfify yaml-mode xterm-color ws-butler winum which-key web-mode web-beautify volatile-highlights vi-tilde-fringe uuidgen use-package toc-org terraform-mode tagedit spaceline smeargle slim-mode shell-pop scss-mode sass-mode rvm ruby-tools ruby-test-mode rubocop rspec-mode robe restart-emacs rbenv rainbow-delimiters pyvenv pytest pyenv-mode py-isort pug-mode projectile-rails popwin pip-requirements persp-mode paradox orgit org-projectile org-present org-pomodoro org-download org-bullets open-junk-file ob-elixir neotree multi-term move-text mmm-mode minitest markdown-toc magit-gitflow macrostep lorem-ipsum livid-mode live-py-mode linum-relative link-hint less-css-mode json-mode js2-refactor js-doc info+ indent-guide hy-mode hungry-delete htmlize hl-todo highlight-parentheses highlight-numbers highlight-indentation hide-comnt help-fns+ helm-themes helm-swoop helm-pydoc helm-projectile helm-mode-manager helm-make helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-c-yasnippet helm-ag google-translate golden-ratio gnuplot gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe git-gutter-fringe+ gh-md fuzzy flyspell-correct-helm flycheck-pos-tip flycheck-mix flycheck-credo flx-ido fill-column-indicator feature-mode fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-magit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-ediff evil-args evil-anzu eval-sexp-fu eshell-z eshell-prompt-extras esh-help emmet-mode elisp-slime-nav dumb-jump diff-hl define-word cython-mode csv-mode company-web company-tern company-statistics company-emacs-eclim company-anaconda column-enforce-mode coffee-mode clean-aindent-mode chruby bundler auto-yasnippet auto-highlight-symbol auto-dictionary auto-compile alchemist aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line ac-ispell))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files (quote ("~/tmp/test.org")))
 '(org-clock-in-switch-to-state "")
 '(org-clock-out-remove-zero-time-clocks t)
 '(org-edit-src-content-indentation 4)
 '(org-edit-timestamp-down-means-later t)
 '(org-extend-today-until 22)
 '(org-indent-indentation-per-level 4)
 '(org-indent-mode-turns-off-org-adapt-indentation t)
 '(org-list-allow-alphabetical t)
 '(org-list-automatic-rules (quote ((checkbox . t) (checkbox . t))))
 '(org-todo-keyword-faces
   (quote
    (("TODO" . "#dc752f")
     ("WAITING" . "#9323cf")
     ("DONE" . "#2774cc")
     ("CANCELLED" . "#07317d"))))
 '(package-selected-packages
   (quote
    (jinja2-mode company-ansible ansible-doc ansible wgrep smex ivy-hydra flyspell-correct-ivy counsel-projectile counsel swiper ivy spotify helm-spotify-plus multi org-redmine unfill mwim yasnippet auto-complete helm-rails ox-gfm org-super-agenda org-dashboard org-journal yasnippet-snippets yasnippet-classic-snippets phpunit phpcbf php-extras php-auto-yasnippets drupal-mode php-mode sql-indent hcl-mode rake inflections pcre2el spinner org-category-capture log4e gntp org-mime skewer-mode simple-httpd multiple-cursors hydra lv parent-mode request haml-mode gitignore-mode fringe-helper git-gutter+ git-gutter flyspell-correct pos-tip flx highlight transient git-commit with-editor iedit anzu goto-chg undo-tree json-snatcher json-reformat diminish csharp-mode web-completion-data tern inf-ruby bind-map bind-key packed anaconda-mode pythonic elixir-mode pkg-info epl powerline alert go-guru go-eldoc flycheck-gometalinter company-go go-mode eclim dash-functional smartparens evil flycheck company projectile helm helm-core avy markdown-mode org-plus-contrib magit magit-popup async f js2-mode dash s dockerfile-mode docker tablist docker-tramp omnisharp yapfify yaml-mode xterm-color ws-butler winum which-key web-mode web-beautify volatile-highlights vi-tilde-fringe uuidgen use-package toc-org terraform-mode tagedit spaceline smeargle slim-mode shell-pop scss-mode sass-mode rvm ruby-tools ruby-test-mode rubocop rspec-mode robe restart-emacs rbenv rainbow-delimiters pyvenv pytest pyenv-mode py-isort pug-mode projectile-rails popwin pip-requirements persp-mode paradox orgit org-projectile org-present org-pomodoro org-download org-bullets open-junk-file ob-elixir neotree multi-term move-text mmm-mode minitest markdown-toc magit-gitflow macrostep lorem-ipsum livid-mode live-py-mode linum-relative link-hint less-css-mode json-mode js2-refactor js-doc info+ indent-guide hy-mode hungry-delete htmlize hl-todo highlight-parentheses highlight-numbers highlight-indentation hide-comnt help-fns+ helm-themes helm-swoop helm-pydoc helm-projectile helm-mode-manager helm-make helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-c-yasnippet helm-ag google-translate golden-ratio gnuplot gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe git-gutter-fringe+ gh-md fuzzy flyspell-correct-helm flycheck-pos-tip flycheck-mix flycheck-credo flx-ido fill-column-indicator feature-mode fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-magit evil-lisp-state evil-indent-plus evil-iedit-state evil-exchange evil-escape evil-ediff evil-args evil-anzu eval-sexp-fu eshell-z eshell-prompt-extras esh-help emmet-mode elisp-slime-nav dumb-jump diff-hl define-word cython-mode csv-mode company-web company-tern company-statistics company-emacs-eclim company-anaconda column-enforce-mode coffee-mode clean-aindent-mode chruby bundler auto-yasnippet auto-highlight-symbol auto-dictionary auto-compile alchemist aggressive-indent adaptive-wrap ace-window ace-link ace-jump-helm-line ac-ispell))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
)
